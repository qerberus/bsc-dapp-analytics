from web3 import Web3
from web3.middleware import geth_poa_middleware
from influxdb import InfluxDBClient
from functools import lru_cache
import pandas as pd

# Binance Smart Chain mainnet API URI
BSC_API_URI = "https://apis.ankr.com/20aec6ababd04d1caaf7a7b1557728f7/7dfba00b50a7fa8271e485011747b460/binance/archive/main"


class Loader(object):
    """Load information about a Dapp contract from the chain."""

    def __init__(self, influx: InfluxDBClient):
        self.STEP = 5
        self.influx = influx
        self.w3 = Web3(Web3.HTTPProvider(BSC_API_URI))
        self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)
        self.blocks = dict()

    def load(
        self,
        address,
        abi,
        event,
        tags,
        fields,
        measurement,
        start_block=0,
    ):
        sc = self.w3.eth.contract(address=address, abi=abi)

        def points_from_w3(ts, data):
            """Transform response data from Web2 into an InfluxDB insert query."""
            json_body = {
                "measurement": measurement,
                "time": ts,
                "tags": {},
                "fields": {},
            }
            for k, v in tags.items():
                json_body["tags"][v] = data[k]

            for k, v in fields.items():
                json_body["fields"][v] = float(data[k] / pow(10,18))

            return json_body

        @lru_cache(maxsize=self.STEP)
        def get_ts(block):
            print(f"get_ts at {block}")
            return pd.Timestamp(self.w3.eth.get_block(block)["timestamp"], unit="s")

        block = start_block
        while block < self.w3.eth.block_number:
            print(f"getting logs from {block} to {block+self.STEP}")
            logs = sc.events[event].getLogs(
                fromBlock=self._hexReprBlock(block),
                toBlock=self._hexReprBlock(block + self.STEP),
            )
            print(f"writing {len(logs)} points")
            self.influx.write_points(
                [
                    points_from_w3(get_ts(event.blockNumber), event.args)
                    for event in logs
                ],
                time_precision="s",
            )
            block += self.STEP

    @staticmethod
    def _hexReprBlock(block):
        return hex(block)
